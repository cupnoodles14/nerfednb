<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="NerfedNB" version="3" date="9/9/2018" >
    
        <Author name="NerfedWar" email="nerfed.war@gmail.com" />
        
        <Description text="Condition based automatic ability swapping." />
        
        <VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
        <Dependencies>
            <Dependency name="EASystem_Tooltips" />
            <Dependency name="EA_ActionBars" />
            <Dependency name="LibSlash" />
        </Dependencies>
        
        <WARInfo>
            <Categories>
                <Category name="ACTION_BARS" />
            </Categories>
            <Careers>
                <Career name="ARCHMAGE" />
                <Career name="BLACKGUARD" />
                <Career name="BLACK_ORC" />
                <Career name="BRIGHT_WIZARD" />
                <Career name="CHOPPA" />
                <Career name="CHOSEN" />
                <Career name="DISCIPLE" />
                <Career name="ENGINEER" />
                <Career name="IRON_BREAKER" />
                <Career name="KNIGHT" />
                <Career name="SHAMAN" />
                <Career name="SQUIG_HERDER" />
                <Career name="MAGUS" />
                <Career name="MARAUDER" />
                <Career name="RUNE_PRIEST" />
                <Career name="SHADOW_WARRIOR" />
                <Career name="SLAYER" />
                <Career name="SORCERER" />
                <Career name="SWORDMASTER" />
                <Career name="WARRIOR_PRIEST" />
                <Career name="WHITE_LION" />
                <Career name="WITCH_ELF" />
                <Career name="WITCH_HUNTER" />
                <Career name="ZEALOT" />
            </Careers>
        </WARInfo>

        <Files>
            <File name="NerfedIcon.xml" />
            <File name="NerfedUtils.lua" />
            <File name="NerfedButtons.lua" />
            <File name="NerfedAPI.lua" />
            <File name="NerfedChecks.lua" />
            <File name="NerfedDecisions.lua" />
            <File name="NerfedEngine.lua" />
            <File name="NerfedMatchMaking.lua" />
            <File name="NerfedMemory.lua" />
            <File name="NerfedTalks.lua" />
            <File name="NerfedUtils.lua" />
            
            
            <File name="Languages\english.dev.lua"/>
            <File name="Languages\english.lua"/>
            <File name="Languages\french.lua"/>
            <File name="Languages\german.lua"/>
            <File name="Languages\italian.lua"/>
            <File name="Languages\japanese.lua"/>
            <File name="Languages\korean.lua"/>
            <File name="Languages\russian.lua"/>
            <File name="Languages\s_chinese.lua"/>
            <File name="Languages\spanish.lua"/>
            <File name="Languages\t_chinese.lua"/>
            
            <File name="UI\NBSBCommon.lua" />
            <File name="UI\Lang.lua" />
            <File name="UI\NBSBRegister.lua" />
            <File name="UI\NBSBActionButton.xml" />
            <File name="UI\NBSBActionButton.lua" />
            <File name="UI\NBSBActionBar.xml" />
            <File name="UI\NBSBActionBar.lua" />
            <File name="UI\NBSBParam.xml" />
            <File name="UI\NBSBParam.lua" />
            <File name="UI\NBSBChecks.xml" />
            <File name="UI\NBSBChecks.lua" />
            <File name="UI\NBSBCore.xml" />
            <File name="UI\NBSBCore.lua" />
            <File name="UI\NBSetup_Save.xml"/>
            <File name="UI\NBSetup_Save.lua"/>
            <File name="UI\NBSBCheckTooltips.xml"/>
            <File name="UI\NBSBCheckTooltips.lua"/>
        </Files>
        
        <SavedVariables>
            <SavedVariable name="NerfedMemory.global" global="true" />
            <SavedVariable name="NerfedMemory.actionDataCache" global="true" />
            <SavedVariable name="NerfedMemory.bindings" />
            <SavedVariable name="NerfedMemory.macros" />
            <SavedVariable name="NerfedMemory.profiles" />
        </SavedVariables>
        
        <OnInitialize>
            <CallFunction name="NerfedButtons.Initialize" />
            <CreateWindow name="NBSBCoreWindow" />
            <CreateWindow name="NBSetup_Save" />
            <CreateWindow name="NBSBCheckTooltip" />
            <CallFunction name="NBSBCore.Initialize" />
        </OnInitialize>
        
        <OnUpdate>
            <CallFunction name="NerfedEngine.OnUpdate" />
            <CallFunction name="NerfedButtons.OnUpdate" />
        </OnUpdate>
        
    </UiMod>
</ModuleFile>

